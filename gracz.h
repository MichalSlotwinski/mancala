#ifndef __GRACZ_H_INCLUDED__
#define __GRACZ_H_INCLUDED__

//czyj ruch
static int tura;

//klasa abstrakcyjna gracz
class gracz
{
protected:
	int house[7];
public:
	int get_house(int i) { return house[i]; }
	void set_house(int i, int ile) { this->house[i] = ile; }
	virtual char* get_nazwa() { return 0; }
	virtual void set_nazwa(char* nazwa) {}
	virtual void wypisz_nazwa() = 0;
	virtual void wypisz_house(int i) = 0;
	virtual void wypisz_bank() = 0;
	int wykonaj_ruch(int pole, int ile, int koniec, gracz* wsk, gracz* wsk2, int przejscie);
	bool czy_wygral();

	gracz() :house{ 4, 4, 4, 4, 4, 4, 0 } {}
	gracz(int* house) :house{ house[0], house[1] , house[2] , house[3] , house[4] ,house[5] , house[6] } {}
	gracz(gracz const &kopia) :house{ kopia.house[0], kopia.house[1] , kopia.house[2] , kopia.house[3] , kopia.house[4] ,kopia.house[5] , kopia.house[6] } {}
	~gracz() {}
};

#endif