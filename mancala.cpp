#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "mancala.h"

//wypisanie planszy
void wypisz(gracz* gracz1, gracz* gracz2)
{
	int i;
	system("cls");
	std::cout << "\t\t\t<--";
	gracz2->wypisz_nazwa();
	std::cout << "\n  \t6.\t5.\t4.\t3.\t2.\t1.\t\n"
		<< "=========================================================\n\n";
	for (i = 5; i >= 0; i--)
		gracz2->wypisz_house(i);
	std::cout << "\t\n"
		<< "\t=========================================\n";
	gracz2->wypisz_bank();
	std::cout << "\t\t\t\t\t\t\t";
	gracz1->wypisz_bank();
	std::cout << "\n\t=========================================\n";
	for (i = 0; i < 6; i++)
		gracz1->wypisz_house(i);
	std::cout << "\t\n"
		<< "\n=========================================================\n"
		<< "\t1.\t2.\t3.\t4.\t5.\t6.\t\n"
		<< "\t\t\t";
	gracz1->wypisz_nazwa();
	std::cout << "-->\n\n\n";
}

//utworzenie gracza ludzkiego i podanie nazwy
czlowiek* podaj_nazwe()
{
	char* nazwa = new char[10];

	std::cout << "Podaj nazwe (10 znakow) : ";
	std::cin >> nazwa;
	czlowiek* nowy_czlowiek = new czlowiek();
	nowy_czlowiek->set_nazwa(nazwa);

	return nowy_czlowiek;
}

//funkcja sumujaca wszystkie pozostale pola do banku po zakonczeniu gry
void rezultat(gracz* wsk1, gracz* wsk2)
{
	int tmp = wsk1->get_house(6);
	for (int i = 0; i < 6; i++)
	{
		tmp += wsk1->get_house(i);
		wsk1->set_house(i, 0);
	}
	wsk1->set_house(6, tmp);

	tmp = wsk2->get_house(6);
	for (int i = 0; i < 6; i++)
	{
		tmp += wsk2->get_house(i);
		wsk2->set_house(i, 0);
	}
	wsk2->set_house(6, tmp);
}

//kto pierwszy rozpoczyna gre
int kto_pierwszy(gracz* gracz1, gracz* gracz2)
{
	int n;
	system("cls");
	std::cout << "Kto zaczyna jako pierwszy ?\n(";
	gracz1->wypisz_nazwa();
	std::cout << ")-1,(";
	gracz2->wypisz_nazwa();
	std::cout << "-2) : Co wybierasz ? : ";
	while (scanf("%d", &n) != 1 || isdigit(n) || (n < 1 || n > 2) || getchar() != '\n')
	{
		std::cout << "Co wybierasz ? : ";
		while (getchar() != '\n');
	}
	if (strncmp(gracz2->get_nazwa(), "KOMPUTER", 8) == 0 && n == 2)
		return 3;
	return n;
}
