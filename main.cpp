#include <ctime>
#include <iostream>
#include "mancala.h"


void main()
{
	//losowanie ruchow komputera wzgledem czasu
	srand(time(NULL));

	//stylowy naglowek
	std::cout << "\t\t\t\tMANCALA\n";
	std::cout << "-------------------------------------------------------------------------------\n\n";
	system("pause");

	//deklaracja zmiennych
	gracz *gracz1, *gracz2;
	int nOpcja, nOpcja1, Pole, Stan = false;
	int przejscie;
	FILE *zapis = NULL;
	enum init { menu, gra }x;
	x = menu;

	//petla zawierajaca menu i sterowanie rozgrywka
	while (true)
	{
		if (x == menu)
		{
			//menu glowne
			system("cls");
			std::cout << "Menu:\n"
				<< "1. Nowa gra\n"
				<< "2. Kontynuuj\n"
				<< "3. Wyjscie\n"
				<< "Co wybierasz ? : ";

			//wybor opcji menu glownego
			while (scanf("%d", &nOpcja) != 1 || isdigit(nOpcja) || (nOpcja <= 0 || nOpcja > 3) || getchar() != '\n')
			{
				std::cout << "Co wybierasz ? : ";
				while (getchar() != '\n');
			}

			//kolejne menu, menu(nowa gra)
			switch (nOpcja)
			{
			case 1:
				system("cls");
				std::cout << "<--(nowa gra)\n"
					<< "1. GRACZ vs GRACZ\n"
					<< "2. GRACZ vs KOMPUTER\n"
					<< "Co wybierasz ? : ";

				//wybor opcji z powyzszego menu
				while (scanf("%d", &nOpcja1) != 1 || isdigit(nOpcja) || (nOpcja1 <= 0 || nOpcja1 > 2) || getchar() != '\n')
				{
					std::cout << "Co wybierasz ? : ";
					while (getchar() != '\n');
				}

				//kolejne menu jesli 1 z menu(nowa gra)
				switch (nOpcja1)
				{
				case 1:
					//czlowiek vs czlowiek
					system("cls");
					std::cout << "<--(gracz vs gracz)\n"
						<< "GRACZ 1\n";

					//podanie nazw graczy
					gracz1 = podaj_nazwe();
					std::cout << "GRACZ 2\n";
					gracz2 = podaj_nazwe();
					x = gra;
					Stan = false;
					break;
				case 2:
					//czlowiek vs komputer
					system("cls");
					std::cout << "<--(gracz vs komputer)\n"
						<< "GRACZ\n";
					//podanie nazwy gracz 1 (komputer przyjmuje nazwe "KOMPUTER")
					gracz1 = podaj_nazwe();
					gracz2 = new komputer();
					x = gra;
					Stan = false;
					break;
				default:
					break;
				}
				break;
			case 2:
				//jesli 2. Kontynuuj z menu glownego
				//zmienne pomocnicze do wczytania danych z pliku
				char ta[10], tb[10];
				int t1[7], t2[7];
				int op;

				//sprawdzenie czy plik istnieje
				//jesli nie, koniec programu (aby utworzyc plik, nalezy wybrac nowa gre, podac nazwy i wykonac jeden ruch)
				zapis = fopen("szybki_zapis.txt", "r");
				if (zapis == NULL)
				{
					nOpcja = 1;
					std::cout << "\nBrak ostatniego zapisu.\n"
						<< "KONIEC PROGRAMU\n\n";
					system("pause");
					exit(0);
				}

				//wczytanie z pliku
				fscanf(zapis, "%d %s", &op, ta);
				for (int i = 0; i < 7; i++)
					fscanf(zapis, "%d ", &t1[i]);
				fscanf(zapis, "%s ", tb);
				for (int i = 0; i < 7; i++)
					fscanf(zapis, "%d ", &t2[i]);
				fscanf(zapis, "%d \n", &tura);
				fclose(zapis);

				//utworzenie graczy danymi z pliku
				gracz1 = new czlowiek(t1, ta);
				nOpcja1 = op;
				if (nOpcja1 == 2)
					gracz2 = new komputer(t2);
				else
					gracz2 = new czlowiek(t2, tb);
				x = gra;
				Stan = false;
				break;
			case 3:
				//jesli 3. Wyjscie z menu glownego, koniec programu
				exit(0);
			default:
				break;
			}
		}

		//jesli nowa gra, to kto pierwszy ?
		if (nOpcja != 2)
			tura = kto_pierwszy(gracz1, gracz2);

		//rozgrywka
		if (x == gra)
		{
			while (!Stan)
			{
				//zmienne wchodzace w sklad ruchu
				int tmp, tmp1;
				//tura - kto wykonuje ruch
				switch (tura)
				{
				case 1:
					//gracz 1
					przejscie = 2;
					//wypisanie planszy
					wypisz(gracz1, gracz2);
					//czy koniec
					Stan = gracz1->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}
					//wczytanie pola od uzytkownika
					std::cout << "(0 - wyjscie)\n";
					gracz1->wypisz_nazwa();
					std::cout << " podaj numer pola : ";
					while (scanf("%d", &Pole) != 1 || isdigit(nOpcja) || (Pole < 0 || Pole > 6) || (gracz1->get_house(Pole - 1)) == 0 || getchar() != '\n')
					{
						std::cout << "Podaj numer pola : ";
						while (getchar() != '\n');
					}
					//jesli uzytkownik wybral 0
					if (Pole == 0)
					{
						std::cout << "KONIEC GRY\n";
						system("pause");
						exit(0);
					}
					//ruch
					tmp = gracz1->get_house(Pole - 1);
					tmp1 = gracz1->wykonaj_ruch(Pole, (gracz1->get_house(Pole - 1)) - 1, 7, gracz1, gracz2, przejscie);
					gracz1->set_house(Pole - 1, (gracz1->get_house(Pole - 1)) - tmp);
					//czy koniec
					Stan = gracz1->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}
					//kto nastepny, w zaleznosci wyboru opcji w menu
					if (tmp1)
						tura = 1;
					else
					{
						if (nOpcja1 == 1)
							tura = 2;
						if (nOpcja1 == 2)
							tura = 3;
					}
					break;

				case 2:
					//gracz 2 (jesli czlowiek)
					przejscie = 2;
					//wypisanie planszy
					wypisz(gracz1, gracz2);
					//czy koniec
					Stan = gracz2->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}
					//wczytanie pola od uzytkownika
					std::cout << "(0 - wyjscie)\n";
					gracz2->wypisz_nazwa();
					std::cout << " podaj numer pola : ";
					while (scanf("%d", &Pole) != 1 || isdigit(nOpcja) || (Pole < 0 || Pole > 6) || (gracz2->get_house(Pole - 1)) == 0 || getchar() != '\n')
					{
						std::cout << "Podaj numer pola : ";
						while (getchar() != '\n');
					}
					//jesli uzytkownik wybral 0
					if (Pole == 0)
					{
						std::cout << "KONIEC GRY\n";
						system("pause");
						exit(0);
					}
					//ruch
					tmp = gracz2->get_house(Pole - 1);
					tmp1 = gracz2->wykonaj_ruch(Pole, (gracz2->get_house(Pole - 1)) - 1, 7, gracz2, gracz1, przejscie);
					gracz2->set_house(Pole - 1, (gracz2->get_house(Pole - 1)) - tmp);
					//czy koniec
					Stan = gracz2->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}

					//kto nastepny, w zaleznosci wyboru opcji w menu
					if (tmp1)
						tura = 2;
					else
						tura = 1;
					break;

				case 3:
					//gracz 2 (jesli komputer)
					przejscie = 2;
					//wypisanie planszy
					wypisz(gracz1, gracz2);
					//czy koniec
					Stan = gracz2->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}
					//komputer losuje pole z przedzialu (1,6)
					Pole = rand() % 6 + 1;
					while (gracz2->get_house(Pole - 1) == 0)
						Pole = rand() % 6 + 1;
					//wypisz(gracz1, gracz2);
					gracz2->wypisz_nazwa();
					std::cout << " wybral pole : " << Pole << "\n";
					system("pause");
					//ruch
					tmp = gracz2->get_house(Pole - 1);
					tmp1 = gracz2->wykonaj_ruch(Pole, (gracz2->get_house(Pole - 1)) - 1, 7, gracz2, gracz1, przejscie);
					gracz2->set_house(Pole - 1, (gracz2->get_house(Pole - 1)) - tmp);
					//wypisz(gracz1, gracz2);
					//czy koniec
					Stan = gracz2->czy_wygral();
					if (Stan)
					{
						rezultat(gracz1, gracz2);
						wypisz_rezultat(gracz1, gracz2);
						x = menu;
						break;
					}

					//kto nastepny, w zaleznosci wyboru opcji w menu
					if (tmp1)
						tura = 3;
					else
						tura = 1;
					break;

				default:
					break;
				}
				if (Stan == true)
					break;
				//TU ZAPIS DO PLIKU
				zapis = fopen("szybki_zapis.txt", "w");
				fprintf(zapis, "%d %s ", nOpcja1, gracz1->get_nazwa());
				for (int i = 0; i < 7; i++)
					fprintf(zapis, "%d ", gracz1->get_house(i));
				fprintf(zapis, "%s ", gracz2->get_nazwa());
				for (int i = 0; i < 7; i++)
					fprintf(zapis, "%d ", gracz2->get_house(i));
				fprintf(zapis, "%d \n", tura);
				fclose(zapis);
				//TU ZAPIS DO PLIKU
			}
			//jesli koniec, usun obiekty
			delete gracz1;
			delete gracz2;
		}
	}
}