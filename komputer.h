#ifndef __KOMPUTER_H_INCLUDED__
#define __KOMPUTER_H_INCLUDED__

#include "gracz.h"

//gracz sterowany przez komputer
class komputer : public gracz
{
protected:
	char* nazwa = "KOMPUTER";
public:
	char* get_nazwa() { return nazwa; }
	void set_nazwa(char* nazwa) { this->nazwa = nazwa; }
	void wypisz_nazwa();
	void wypisz_house(int i);
	void wypisz_bank();

	komputer() :gracz() {}
	komputer(int* house) : gracz(house) {}
	~komputer() {}
};

#endif