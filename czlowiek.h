#ifndef __CZLOWIEK_H_INCLUDED__
#define __CZLOWIEK_H_INCLUDED__

#include "gracz.h"

//gracz sterowanu przez czlowieka
class czlowiek : public gracz
{
protected:
	char* nazwa;
public:
	char* get_nazwa() { return nazwa; }
	void set_nazwa(char* nazwa) { this->nazwa = nazwa; }
	void wypisz_nazwa();
	void wypisz_house(int i);
	void wypisz_bank();

	czlowiek() :nazwa("GRACZ ?"), gracz() {}
	czlowiek(int* house, char* nazwa) :nazwa(nazwa), gracz(house) {}
	~czlowiek() {}
};

#endif