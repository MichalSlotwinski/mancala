#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "gracz.h"


//funkcja wykonujaca ruch
//zwraca 0 - nastepny gracz, 1 - dodatkowy ruch
int gracz::wykonaj_ruch(int pole, int ile, int koniec, gracz* wsk1, gracz* wsk2, int przejscie)
{
	//przejscie decyduje czy pominac bank czy nie, w zaleznosci ktore jest to przejscie w trakcie jednej tury
	if (przejscie % 2 != 0)
		koniec = 6;
	else
		koniec = 7;

	//petla
	for (int i = pole; (i < koniec) && (ile >= 0); i++)
	{
		if (wsk1->house[5] == 2 && pole == 6)
		{
			wsk1->house[6]++;
		}
		else
			wsk1->house[i]++;
		//dodatkowy ruch
		if (ile <= 0 && i == 6)
		{
			std::cout << "DODATKOWY RUCH !!\n";
			system("pause");
			return 1;
		}
		//bicie
		if (ile <= 0 && wsk2->house[abs(i - 5)] > 0 && wsk1->house[i] == 1 && przejscie % 2 == 0)
		{
			wsk1->house[6] += wsk2->house[abs(i - 5)];
			wsk1->house[6] += wsk1->house[i];
			wsk2->house[abs(i - 5)] = 0;
			wsk1->house[i] = 0;
			return 0;
		}

		//wyjatki
		if (i == 6 && ile == 1 && przejscie % 2 == 0)
		{
			wsk2->house[0]++;
			return 0;
		}
		if (i == 5 && ile == 1 && przejscie % 2 != 0)
		{
			break;
		}
		if (przejscie % 2 != 0 && pole == 6 && 8 - ile == 9)
			wsk1->house[1]++;
		ile--;
	}

	//rekurencyje wywolanie funkcji ruchu
	if (ile > 0)
	{
		przejscie++;
		wykonaj_ruch(0, ile, koniec, wsk2, wsk1, przejscie);
	}
	return 0;
}

//czy koniec
bool gracz::czy_wygral()
{
	int licz = 0;
	for (int i = 0; i < 6; i++)
	{
		licz += house[i];
	}
	if (licz == 0)
		return true;
	else
		return false;
}