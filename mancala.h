#ifndef __MANCALA_H_INCLUDED__
#define __MANCALA_H_INCLUDED__

#include "gracz.h"
#include "czlowiek.h"
#include "komputer.h"

//wypisanie planszy
void wypisz(gracz* gracz1, gracz* gracz2);

//utworzenie gracza ludzkiego i podanie nazwy
czlowiek* podaj_nazwe();

//funkcja sumujaca wszystkie pozostale pola do banku po zakonczeniu gry
void rezultat(gracz* wsk1, gracz* wsk2);

//szablon funkcji wypisujacej rezultat na koniec gry
template <class TYP1, class TYP2>
void wypisz_rezultat(TYP1 wsk1, TYP2 wsk2)
{
	if (wsk1->get_house(6) > wsk2->get_house(6))
	{
		wypisz(wsk1, wsk2);
		std::cout << "Zwyciezyl ";
		wsk1->wypisz_nazwa();
		std::cout << "\n\nGRATULACJE !!!\n";
		system("pause");
	}
	else if (wsk1->get_house(6) < wsk2->get_house(6))
	{
		wypisz(wsk1, wsk2);
		std::cout << "Zwyciezyl ";
		wsk2->wypisz_nazwa();
		std::cout << "\n\nGRATULACJE !!!\n";
		system("pause");
	}
	else if (wsk1->get_house(6) == wsk2->get_house(6))
	{
		wypisz(wsk1, wsk2);
		std::cout << "\n\nREMIS\n";
		system("pause");
	}
}

//kto pierwszy rozpoczyna gre
int kto_pierwszy(gracz* gracz1, gracz* gracz2);

#endif